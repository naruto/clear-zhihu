/*
 * 这是一张 JavaScript 代码草稿纸。
 *
 * 输入一些 JavaScript，然后可点击右键或从“执行”菜单中选择：
 * 1. 运行 对选中的文本求值(eval) (Ctrl+R)；
 * 2. 查看 对返回值使用对象查看器 (Ctrl+I)；
 * 3. 显示 在选中内容后面以注释的形式插入返回的结果。 (Ctrl+L)
 */
/*退知乎，删回答，保智商*/

//新的回答
function newQuestion(qid, title) {
        var obj = new Object();
        obj.qid = qid.toString();
        obj.title = title.toString();
        return obj;
}

//新回答的表单数据
function newAnswerFormData(question) {
        var post_content = new Object();
        post_content.id = question.qid;
        post_content.content = '退知乎，删回答，保智商。';
        post_content.field_name = 'content';
        post_content._xsrf = getCookie('_xsrf');
        return post_content;
}

function parseId(path) {
        var x = path.split('/');
        return {
                q: x[0],
                a: x[2]
        };
}
function getCookie(name) {
        if (!document.cookie.contains(name))
               return undefined;
        var i0 = document.cookie.lastIndexOf(name);
        var i1 = document.cookie.indexOf('=', i0) + 1;
        var i2 = document.cookie.indexOf(';', i1);
        return document.cookie.substring(i1, i2);
}

//准备待修改的回答的列表
function getAnswerList(questions) {
        var str = 'question/';
        //var len = questions[0].href.indexOf(str) + str.length;
        var count = questions.length;
        var list = new Array();
        for (let i = 0; i < count; i++) {
                //let path = questions[i].href.substring(len);
                //let keys = parseId(path);
                list.push(newQuestion(questions[i].dataset.id, 
                                    questions[i].innerHTML));
        }
        return list;
}

//修改所有问题的回答
function forEachAnswer(answer_list) {
        var count = answer_list.length;
        for (let i = 0; i < count; i++) {
                changeAnswer(answer_list[i]);
                unfollowQuestion(answer_list[i]);
        }
}

//修改答案
function changeAnswer(question) {
        const post_url = 'https://www.zhihu.com/answer/content';
        $.post(post_url, newAnswerFormData(question), function (text, status) {
                console.log('[' + status + ']修改答案 ：' + question.title);
        }, 'json');
}

//取消关注问题
function unfollowQuestion(question) {
        var post_url = "https://www.zhihu.com/node/QuestionFollowBaseV2";
        $.post(post_url, newUnfolllowFormData(question.qid), function (text, status) {
                console.log('[' + status + ']取消关注问题 ：' + question.title);
        }, 'json');
}

//取消关注问题的表单数据
function newUnfolllowFormData(qid) {
        var data = new Object();
        data.method = 'unfollow_question';
        data.params = '{"question_id":"' + qid.toString() + '"}';
        data._xsrf = getCookie('_xsrf');
        return data;
}

//清除回答
function clearAnswers() {
        if (!document.cookie.contains('_xsrf')) {
                alert('_xsrf的cookie值不存在，请刷新页面');
                return;
        }
        var selector = '#zh-profile-answer-list .zm-item .question_link';
        var questions = document.querySelectorAll(selector);
        var list = getAnswerList(questions);
        forEachAnswer(list);
}

clearAnswers();